package Core;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileManager {

    private static String fileContent;
    public static String FILE_PATH = "../res/accounts.txt";

    public static boolean canOpen(String filePath)
    {
        File file = new File(filePath);
        return file.exists();
    }

    public static String readFile(String filePath) {
        String content = null;
        File file = new File(filePath);
        FileReader reader = null;

        try {
            reader = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            content = new String(chars);
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (reader != null)
        {
            try {
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        fileContent = content;
        return content;
    }

    public static void createFile(String filePath) {
        Path path = Paths.get(filePath);

        try {
            Files.createFile(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
