package windowing.panels;

import accounts.AccountsManager;

import javax.swing.*;
import java.awt.*;

public class HeaderPanel extends JPanel {

    JLabel moneyLabel;

    public HeaderPanel()
    {
        if (AccountsManager.accounts.size() == 0)
            moneyLabel = new JLabel("No Account");
        else
            moneyLabel = new JLabel(AccountsManager.getMainAccount().balance + "€");

        setPreferredSize(new Dimension(50, 50));
        setBackground(Color.ORANGE);
        add(moneyLabel);
    }

    public void refresh()
    {
        if (AccountsManager.accounts.size() != 0)
            moneyLabel.setText(AccountsManager.getMainAccount().balance + "€");
    }

}
