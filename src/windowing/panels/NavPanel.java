package windowing.panels;

import Core.MainApp;
import events.ButtonEvents;
import windowing.AppMenu;

import javax.swing.*;
import java.awt.*;

public class NavPanel extends JPanel {

    private JButton homeButton;
    private JButton expenseButton;
    private JButton accountsButton;
    private JButton settingsButton;
    private JLabel versionLabel;

    public NavPanel()
    {
        initPanel();
        initButtons();

        versionLabel = new JLabel("V" + MainApp.VERSION);

        addComponents();
    }

    private void addComponents() {
        add(homeButton);
        add(expenseButton);
        add(accountsButton);
        add(settingsButton);
        add(versionLabel);
    }

    private void initPanel() {
        setPreferredSize(new Dimension(75, 75));
        setBackground(Color.CYAN);
    }

    private void initButtons()
    {
        homeButton = new JButton("Home");
        homeButton.setPreferredSize(new Dimension(75, 75));
        homeButton.setMargin(new Insets(0, 0, 0, 0));
        homeButton.addActionListener(ButtonEvents.changeMenu(AppMenu.Home));
        homeButton.setBackground(Color.BLACK);
        homeButton.setForeground(Color.WHITE);
        homeButton.setOpaque(true);
        homeButton.setBorder(BorderFactory.createEmptyBorder());

        expenseButton = new JButton("Expenses");
        expenseButton.setPreferredSize(new Dimension(75, 75));
        expenseButton.setMargin(new Insets(0, 0, 0, 0));
        expenseButton.addActionListener(ButtonEvents.changeMenu(AppMenu.Expense));
        expenseButton.setBackground(Color.BLACK);
        expenseButton.setForeground(Color.WHITE);
        expenseButton.setOpaque(true);
        expenseButton.setBorder(BorderFactory.createEmptyBorder());

        accountsButton = new JButton("Accounts");
        accountsButton.setPreferredSize(new Dimension(75, 75));
        accountsButton.setMargin(new Insets(0, 0, 0, 0));
        accountsButton.addActionListener(ButtonEvents.changeMenu(AppMenu.Accounts));
        accountsButton.setBackground(Color.BLACK);
        accountsButton.setForeground(Color.WHITE);
        accountsButton.setOpaque(true);
        accountsButton.setBorder(BorderFactory.createEmptyBorder());

        settingsButton = new JButton("Settings");
        settingsButton.setPreferredSize(new Dimension(75, 75));
        settingsButton.setMargin(new Insets(0, 0, 0, 0));
        settingsButton.addActionListener(ButtonEvents.changeMenu(AppMenu.Settings));
        settingsButton.setBackground(Color.BLACK);
        settingsButton.setForeground(Color.WHITE);
        settingsButton.setOpaque(true);
        settingsButton.setBorder(BorderFactory.createEmptyBorder());
    }

}
