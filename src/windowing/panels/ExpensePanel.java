package windowing.panels;

import Core.FileManager;
import accounts.AccountsManager;
import accounts.Expense;
import events.ButtonEvents;
import windowing.Window;

import javax.swing.*;
import java.awt.*;
import java.util.Date;

public class ExpensePanel extends JPanel {

    private Expense expense;

    private JLabel amountLabel;
    private JLabel wrongAmountLabel;
    private JLabel noAccountLabel;

    private JTextField amountField;

    private JButton submitButton;

    public ExpensePanel()
    {
        initPanel();
        initComponents();
        addComponents();
    }

    private void initPanel() {
        setMinimumSize(new Dimension(250, 250));
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setName("Expense");
    }

    private void initComponents()
    {
        amountLabel = new JLabel("- Balance -");
        noAccountLabel = new JLabel("Please create a first account before");
        wrongAmountLabel = new JLabel("Not a valid balance");

        wrongAmountLabel.setVisible(false);
        noAccountLabel.setVisible(false);

        amountField = new JTextField();

        submitButton = new JButton("Submit");
        submitButton.setPreferredSize(new Dimension(75, 50));
        submitButton.setMargin(new Insets(0, 0, 0, 0));
        submitButton.setBackground(Color.BLACK);
        submitButton.setForeground(Color.WHITE);
        submitButton.addActionListener(ButtonEvents.submitNewExpense(this));
    }

    private void addComponents()
    {
        add(noAccountLabel);
        add(amountLabel);
        add(amountField);
        add(wrongAmountLabel);
        add(submitButton);
    }

    public void submitNewExpense()
    {
        boolean correct = true;
        float balance = 0.0f;

        wrongAmountLabel.setVisible(false);
        noAccountLabel.setVisible(false);

        if (!FileManager.canOpen(FileManager.FILE_PATH))
        {
            noAccountLabel.setVisible(true);
            correct = false;
        }

        try {
            balance = Float.parseFloat(amountField.getText());
        } catch(Exception e) {
            wrongAmountLabel.setVisible(true);
            correct = false;
        }

        if(correct)
        {
            expense = new Expense(balance, new Date());
            AccountsManager.addExpense(expense);
            Window.refresh();
        }
    }

}
