package windowing;

public enum AppMenu {
    Home, Expense, Accounts, NewAccount, Settings
}
