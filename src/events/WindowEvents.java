package events;

import Core.FileManager;
import accounts.AccountsManager;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowEvents {

    public static WindowAdapter saveAccountsOnClose() {return new SaveAccountsOnCloseWindowEvent();}

    public static class SaveAccountsOnCloseWindowEvent extends WindowAdapter
    {
        @Override
        public void windowClosing(WindowEvent e)
        {
            if (FileManager.canOpen(FileManager.FILE_PATH))
                AccountsManager.saveData();
            System.exit(0);
        }
    }

}
