package accounts;

import java.util.Date;

public class Expense {

    public float amount;
    public Date date;

    public Expense(float _amount, Date _date)
    {
        amount = _amount;
        date = _date;
    }

}
