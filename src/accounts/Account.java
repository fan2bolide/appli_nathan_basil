package accounts;

import java.util.ArrayList;

public class Account {

    public String name;
    public float balance;
    public ArrayList<Expense> expenses;

    public Account(String _name, float _amount)
    {
        name = _name;
        balance = _amount;
        expenses = new ArrayList<>();
    }

    public void addExpense(Expense expense)
    {
        expenses.add(expense);
    }

}
