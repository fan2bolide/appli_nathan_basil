package windowing.panels;

import accounts.Account;
import accounts.AccountsManager;
import events.ButtonEvents;
import windowing.AppMenu;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.util.ArrayList;

public class AccountsPanel extends JPanel {

    private ArrayList<JButton> accounts;

    private JButton addButton;

    public AccountsPanel()
    {
        initPanel();
        initComponents();
        addComponents();
    }

    private void initPanel() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setName("Accounts");
    }

    private void initComponents()
    {
        accounts = new ArrayList<>();

        addButton = new JButton("New");
        addButton.setMinimumSize(new Dimension(100, 100));
        addButton.setMargin(new Insets(0, 0, 0, 0));
        addButton.setBackground(Color.gray);
        addButton.setForeground(Color.WHITE);
        addButton.addActionListener(ButtonEvents.changeMenu(AppMenu.NewAccount));
    }

    private void addComponents()
    {
        add(addButton);
    }

    public void refresh()
    {
        for (Account a : AccountsManager.accounts)
        {
            boolean exists = false;

            for (JButton b : accounts)
                if (b.getText().equals(a.name))
                    exists = true;

            if (exists)
                continue;

            JButton button = new JButton(a.name);
            button.setMinimumSize(new Dimension(75, 50));
            button.setMargin(new Insets(0, 0, 0, 0));
            button.setBackground(Color.lightGray);
            button.setForeground(Color.WHITE);
            button.addActionListener(ButtonEvents.setMainAccount(a));

            accounts.add(button);
            add(button);
        }
    }

}
