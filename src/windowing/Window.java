package windowing;

import events.WindowEvents;
import windowing.panels.*;

import javax.swing.*;
import java.awt.*;

public class Window {

    private static JFrame frame;

    private static String title;

    private static HeaderPanel headerPanel;

    private static NavPanel navPanel;

    private static JPanel contentPanel;
    private static HomePanel homePanel;
    private static ExpensePanel expensePanel;
    private static AccountsPanel accountsPanel;
    private static NewAccountPanel newAccountPanel;
    private static SettingsPanel settingsPanel;

    public static void init(String _title)
    {
        title = _title;
        initFrame();
        initPanels();
        addPanelsToFrame();
        displayWindow();
    }

    public static void setMenu(AppMenu menu)
    {
        CardLayout cardLayout = (CardLayout)contentPanel.getLayout();
        cardLayout.show(contentPanel, menu.toString());
    }

    private static void initFrame()
    {
        frame = new JFrame();
        frame.setTitle(title);
        frame.setDefaultLookAndFeelDecorated(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addWindowListener(WindowEvents.saveAccountsOnClose());
        frame.setMinimumSize(new Dimension(500, 500));
        frame.setLayout(new BorderLayout());
    }

    private static void initPanels()
    {
        headerPanel = new HeaderPanel();

        navPanel = new NavPanel();

        homePanel = new HomePanel("basil");
        expensePanel = new ExpensePanel();
        accountsPanel = new AccountsPanel();
        newAccountPanel = new NewAccountPanel();
        settingsPanel = new SettingsPanel();

        contentPanel = new JPanel();
        contentPanel.setLayout(new CardLayout());
        contentPanel.add(homePanel, homePanel.getName());
        contentPanel.add(expensePanel, expensePanel.getName());
        contentPanel.add(accountsPanel, accountsPanel.getName());
        contentPanel.add(newAccountPanel, newAccountPanel.getName());
        contentPanel.add(settingsPanel, settingsPanel.getName());
    }

    private static void addPanelsToFrame()
    {
        frame.add(headerPanel, BorderLayout.NORTH);
        frame.add(navPanel, BorderLayout.WEST);
        frame.add(contentPanel, BorderLayout.CENTER);
    }

    private static void displayWindow()
    {
        frame.pack();
        frame.setVisible(true);
    }

    public static void refresh()
    {
        headerPanel.refresh();
        homePanel.refresh();
        accountsPanel.refresh();
    }
}
