package accounts;

import Core.FileManager;
import windowing.Window;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

public class AccountsManager {

    public static ArrayList<Account> accounts;
    public static Account mainAccount;

    public static void init()
    {
        accounts = new ArrayList<>();
        mainAccount = null;
    }

    public static void addNewAccount(Account newAccount)
    {
        accounts.add(newAccount);
        mainAccount = newAccount;
    }

    public static Account getMainAccount()
    {
        return mainAccount;
    }

    public static void addExpense(Expense expense)
    {
        mainAccount.expenses.add(expense);
        mainAccount.balance -= expense.amount;
    }

    public static void setMainAccount(Account _account)
    {
        mainAccount = _account;
        Window.refresh();
    }

    public static void saveData()
    {
        String result = "";

        for (Account a : accounts)
        {
            result += "A " + a.name + "\n";
            result += a.balance + "\n";
            for (Expense e : a.expenses)
            {
                result += "E " + a.name + "\n";
                result += e.amount + "\n";
                result += e.date.toString() + "\n";
            }
        }

        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(FileManager.FILE_PATH), "utf-8"))) {
            writer.write(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadData()
    {
        String data = FileManager.readFile(FileManager.FILE_PATH);

        int charNum = data.length();
        int i = 0;

        while (i < charNum)
        {
            char c = data.charAt(i);
            if (c == 'A')
            {
                i += 2;
                String name = "";
                String balanceStr = "";
                float balance;

                while (data.charAt(i) != '\n')
                {
                    name += data.charAt(i);
                    i++;
                }
                i++;
                while (data.charAt(i) != '\n')
                {
                    balanceStr += data.charAt(i);
                    i++;
                }
                i++;
                balance = Float.parseFloat(balanceStr);
                addNewAccount(new Account(name, balance));
            }

            if (c == 'E')
            {
                i += 2;
                String accountName = "";
                String amountStr = "";
                String date = "";
                float amount;

                while (data.charAt(i) != '\n')
                {
                    accountName += data.charAt(i);
                    i++;
                }
                i++;
                while (data.charAt(i) != '\n')
                {
                    amountStr += data.charAt(i);
                    i++;
                }
                i++;
                while (data.charAt(i) != '\n')
                {
                    date += data.charAt(i);
                    i++;
                }
                i++;
                amount = Float.parseFloat(amountStr);

                for (Account a : accounts)
                {
                    if (Objects.equals(a.name, accountName))
                    {
                        try {
                            a.expenses.add(new Expense(amount, new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", new Locale("en_US")).parse(date)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        Window.refresh();
    }
}
