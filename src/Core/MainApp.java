package Core;

import accounts.AccountsManager;
import windowing.Window;

public class MainApp {

    public static String VERSION = "0.0.4";

    public void init()
    {
        AccountsManager.init();
        start();
    }

    public void start()
    {
        Window.init("Thunes");
        if (FileManager.canOpen(FileManager.FILE_PATH))
        {
            System.out.println("can open !");
            AccountsManager.loadData();
        }
    }

}
