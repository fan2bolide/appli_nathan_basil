package windowing.panels;

import Core.FileManager;
import accounts.Account;
import accounts.AccountsManager;
import events.ButtonEvents;
import windowing.AppMenu;
import windowing.Window;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;

public class NewAccountPanel extends JPanel {

    private Account newAccount;

    private JLabel nameLabel;
    private JLabel amountLabel;
    private JLabel wrongNameLabel;
    private JLabel wrongAmountLabel;
    private JLabel accountAlreadyExistLabel;

    private JTextField nameField;
    private JTextField amountField;

    private JPanel bottomNav;
    private JButton submitButton;
    private JButton backButton;

    public NewAccountPanel()
    {
        initPanel();
        initComponents();
        addComponents();
    }

    private void initPanel() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setName("NewAccount");
    }

    private void initComponents()
    {
        nameLabel = new JLabel("- Account's name -");
        amountLabel = new JLabel("- Balance -");
        wrongNameLabel = new JLabel("Please enter a name");
        wrongAmountLabel = new JLabel("Not a valid balance");
        accountAlreadyExistLabel = new JLabel("Account already exist");

        wrongNameLabel.setVisible(false);
        wrongAmountLabel.setVisible(false);
        accountAlreadyExistLabel.setVisible(false);

        nameField = new JTextField();
        nameField.setPreferredSize(new Dimension(100, 25));
        amountField = new JTextField();
        amountField.setPreferredSize(new Dimension(100, 25));

        bottomNav = new JPanel();

        backButton = new JButton("Back");
        backButton.setPreferredSize(new Dimension(75, 50));
        backButton.setMargin(new Insets(0, 0, 0, 0));
        backButton.setBackground(Color.BLACK);
        backButton.setForeground(Color.WHITE);
        backButton.addActionListener(ButtonEvents.changeMenu(AppMenu.Accounts));

        submitButton = new JButton("Submit");
        submitButton.setPreferredSize(new Dimension(75, 50));
        submitButton.setMargin(new Insets(0, 0, 0, 0));
        submitButton.setBackground(Color.BLACK);
        submitButton.setForeground(Color.WHITE);
        submitButton.addActionListener(ButtonEvents.submitNewAccount(this));

        bottomNav.add(backButton);
        bottomNav.add(submitButton);
    }

    private void addComponents()
    {
        add(nameLabel);
        add(nameField);
        add(wrongNameLabel);
        add(accountAlreadyExistLabel);
        add(amountLabel);
        add(amountField);
        add(wrongAmountLabel);
        add(bottomNav);
    }

    public void submitNewAccount()
    {
        boolean correct = true;
        float balance = 0.0f;

        wrongAmountLabel.setVisible(false);
        wrongNameLabel.setVisible(false);
        accountAlreadyExistLabel.setVisible(false);

        if (nameField.getText().chars().count() == 0)
        {
            wrongNameLabel.setVisible(true);
            correct = false;
        }

        for (Account a : AccountsManager.accounts)
        {
            if (a.name.equals(nameField.getText()))
            {
                accountAlreadyExistLabel.setVisible(true);
                correct = false;
            }
        }

        try {
            balance = Float.parseFloat(amountField.getText());
        } catch(Exception e) {
            wrongAmountLabel.setVisible(true);
            correct = false;
        }

        if(correct)
        {
            newAccount = new Account(nameField.getText(), balance);
            AccountsManager.addNewAccount(newAccount);
            if (!FileManager.canOpen(FileManager.FILE_PATH))
            {
                FileManager.createFile(FileManager.FILE_PATH);
            }
            Window.refresh();
        }
    }

}
