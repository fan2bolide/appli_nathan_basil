package events;

import accounts.Account;
import accounts.AccountsManager;
import windowing.AppMenu;
import windowing.Window;
import windowing.panels.ExpensePanel;
import windowing.panels.NewAccountPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;

public class ButtonEvents {

    public static Action changeMenu(AppMenu menu) { return new ChangeMenuButtonEvent(menu); }
    public static Action submitNewAccount(NewAccountPanel _newAccountPanel) { return new SubmitNewAccountButtonEvent(_newAccountPanel); }
    public static Action submitNewExpense(ExpensePanel _expensePanel) { return new SubmitNewExpenseButtonEvent(_expensePanel); }
    public static Action setMainAccount(Account _account) { return new SetMainAccountButtonEvent(_account); }

    public static class BasicEvent implements Action
    {

        @Override
        public Object getValue(String key) {
            return null;
        }

        @Override
        public void putValue(String key, Object value) {

        }

        @Override
        public void setEnabled(boolean b) {

        }

        @Override
        public boolean isEnabled() {
            return false;
        }

        @Override
        public boolean accept(Object sender) {
            return Action.super.accept(sender);
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {

        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Button action not implemented");
        }
    }

    public static class ChangeMenuButtonEvent extends BasicEvent
    {
        AppMenu menu;

        public ChangeMenuButtonEvent(AppMenu _menu)
        {
            menu = _menu;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Window.setMenu(menu);
        }
    }

    public static class SubmitNewAccountButtonEvent extends BasicEvent
    {
        NewAccountPanel newAccountPanel;

        public SubmitNewAccountButtonEvent(NewAccountPanel _newAccountPanel)
        {
            newAccountPanel = _newAccountPanel;
        }

        @Override
        public void actionPerformed(ActionEvent e) { newAccountPanel.submitNewAccount();
        }
    }

    public static class SubmitNewExpenseButtonEvent extends BasicEvent
    {
        ExpensePanel expensePanel;

        public SubmitNewExpenseButtonEvent(ExpensePanel _expensePanel)
        {
            expensePanel = _expensePanel;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            expensePanel.submitNewExpense();
        }
    }

    public static class SetMainAccountButtonEvent extends BasicEvent
    {
        Account account;

        public SetMainAccountButtonEvent(Account _account)
        {
            account = _account;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            AccountsManager.setMainAccount(account);
        }
    }

}
